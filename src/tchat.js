import Bot from './bot';

const Tchat = class {
  constructor(bots) {
    this.el = document.querySelector('#app');
    this.bots = this.createBots(bots);
  }

  renderHeader() {
    return `
        <header>
            <nav class="navbar navbar-dark bg-dark">
                <span class="navbar-brand mb-0 h1">Projet Chatbox</span>
            </nav>
        </header>
        `;
  }

  renderContainer() {
    return `
    <main class="container-fluid">
      <div class="row">
      ${this.renderBotsList()}
      ${this.renderContentMessages()}
      </div>
    </main>
    `;
  }

  renderMessageSend(message) {
    const date = new Date();
    return `
      <div class = "row">
        <div class="col-6"></div>
          <div class="col-6">
            <div class="card">
              <div class="card-header">
                <img width="30" src="https://upload.wikimedia.org/wikipedia/commons/thumb/6/6e/Breezeicons-actions-22-im-user.svg/1200px-Breezeicons-actions-22-im-user.svg.png" class="img-fluid rounded-circle border border-secondary border-2"/>
                <span class="ms-3 h4">lulu</span>
              </div>
              <div class="card-body">
                <h5 class="card-title">${date.toLocaleString()}</h5>
                <p class="card-text">${message}</p>
              </div>
          </div>
        </div>
      </div>
    `;
  }

  renderMessageReceived(message) {
    const date = new Date();
    return `
      <div class = "row mt-3">
        <div class="col-6">
          <div class="card">
            <div class="card-header">
              lulu bot
            </div>
            <div class="card-body">
              <h5 class="card-title">${date.toLocaleString()}</h5>
              <p class="card-text">${message}</p>
            </div>
          </div>
        </div>
        <div class="col-6"></div>
      </div>
    `;
  }

  renderInputMessage() {
    return `
      <div id="input-message" class="row">
        <div class="col-12">
          <form class="row g-2">
            <div class="col-10">
              <input type="text" class="form-control" id="inputPassword2" placeholder="your message">
            </div>
            <div class="col-2 d-grid gap-2">
              <button type="submit" class="btn btn-dark mb-3">Send</button>
            </div>
          </form>
        </div>
      </div>
    `;
  }

  renderContentMessages() {
    return `
    <section id="content-message" class="col-9">
      <div id="messages" class = "row"></div>
      ${this.renderInputMessage()}
    </section>
    `;
  }

  renderBotsList() {
    return `
    <section id = "bot-list" class = "col-3 bg-dark">
      ${this.bots.map((bot) => this.renderBot(bot.entity)).join('')}
    </section>
    `;
  }

  renderBot(data) {
    const {
      id,
      name,
      avatar,
      countMessage
    } = data;
    return `
    <div data-id="${id}"class="row">
      <div class="col-3">
        <img width="80" src="${avatar}" class="rounded-circle border border-dark border-2" alt="${name}"/>
      </div>
      <div class="col-7 pt-2">
      <h4 class="h4">${name}</h4>
      </div>
      <div class="col-2 pt-4">
          <span class="badge bg-primary rounded-pill">${countMessage}</span>
      </div>
    </div>
    <hr />
    `;
  }

  addCountMessage(el) {
    const badge = el.querySelector('.badge');
    badge.textContent = parseInt(badge.textContent, 10) + 1;
  }

  sendMessage() {
    const messagesEl = document.querySelector('#messages');
    const inputEl = document.querySelector('#input-message input');
    const buttonEl = document.querySelector('#input-message button');

    buttonEl.addEventListener('click', (e) => {
      e.preventDefault();
      const { value } = inputEl;
      messagesEl.scrollTop = messagesEl.scrollHeight;
      messagesEl.innerHTML += this.renderMessageSend(value);
      inputEl.value = '';

      this.searchActionBybot(value);
    });
  }

  createBots(bots) {
    return bots.map((bot) => new Bot(bot));
  }

  searchActionBybot(value) {
    const messagesEl = document.querySelector('#messages');
    const actions = this.bots.map((bot) => bot.findActionByValue(value));
    actions.forEach((message) => {
      if (message) {
        messagesEl.innerHTML += this.renderMessageReceived(message);
      }
    });
  }

  run() {
    this.el.innerHTML += this.renderHeader();
    this.el.innerHTML += this.renderContainer();
    this.sendMessage();
  }
};

export default Tchat;
