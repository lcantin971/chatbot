import Tchat from './tchat';

import './index.scss';

const bots = [{
  id: '1',
  name: 'Kitty',
  avatar: 'https://upload.wikimedia.org/wikipedia/commons/thumb/6/6e/Breezeicons-actions-22-im-user.svg/1200px-Breezeicons-actions-22-im-user.svg.png" class="img-fluid rounded-circle border border-secondary border-2',
  countMessage: 0,
  actions: [{
    name: 'hello',
    keywords: ['hello', 'bonjour'],
    action: () => 'bonjour lulu'
  }]
}, {
  id: '2',
  name: 'Sansa Stark',
  avatar: 'https://upload.wikimedia.org/wikipedia/commons/thumb/6/6e/Breezeicons-actions-22-im-user.svg/1200px-Breezeicons-actions-22-im-user.svg.png" class="img-fluid rounded-circle border border-secondary border-2',
  countMessage: 0,
  actions: [{
    name: 'hello',
    keywords: ['hello', 'bonjour'],
    action: () => 'bonjour lulu'
  }]
}];

const tchat = new Tchat(bots);

tchat.run();
